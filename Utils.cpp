#include <assert.h>
#include <iostream>

#include "Utils.h"

using namespace std;

void GetText(const string& msg, string& val, bool firstCapital)
{
	assert(val.empty());
	assert(!msg.empty());
	bool inputOK;
	do {
		inputOK = true;
		cout << "\n\n" << msg;
		cin >> val;
		if (cin.fail() || val.empty())
		{
			cout << "\nBad input, try again";
			cin.clear();
			cin.ignore(10000, '\n');
			inputOK = false;
		}
	} while (!inputOK);

	if (!val.empty())
		val[0] = toupper(val[0]);
}


int GetNum(const string& msg, int min, int max)
{
	assert(!msg.empty());
	int val = -1;
	do {
		cout << "\n\n" << msg;
		cin >> val;
		if (cin.fail() || val<min || val>max)
		{
			cout << "\nBad input, try again";
			cin.clear();
			cin.ignore(10000, '\n');
			val = -1;
		}
	} while (val == -1);
	return val;
}
