#include <iostream>
#include <string>
#include <assert.h>

#include "Student.h"

using namespace std;

/*
* Simple example of using structs, but this time functions/structs have been moved into different modules
* (pairs of .cpp and .h files)
*/

int main()
{
	cout << "\n\nStructs";

	Student s;
	s.GetDetails();
	s.ShowDetails();

	system("pause");
	return 0;
}

