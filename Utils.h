#pragma once

#include <string>

/*
Get some text from the user
msg			- IN some instructions to display
val			- OUT user entry
firstCapital- capitalise first letter
*/
void GetText(const std::string&, std::string&, bool);

/*
Get a number from the user
msg		- IN some instructions to display
min/max	- IN upper and lower limits
RETURN	- the number entered
*/
int GetNum(const std::string&, int, int);

