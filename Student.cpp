#include <iostream>

#include "Student.h"
#include "Utils.h"

using namespace std;

void Student::GetDetails() {
	fName = "";
	sName = "";
	IDNum = 0;
	GetText("Enter your first name and press return:", fName, true);
	GetText("Enter your surname name and press return:", sName, true);
	IDNum = GetNum("Enter your student ID (a number between 1000 and 9999:", 1000, 9999);
}

void Student::ShowDetails()
{
	cout << "\n\nStudent summary";
	cout << "\n---------------";
	cout << "\nName: " << fName << " " << sName;
	cout << "\nID: " << IDNum;
	cout << "\n\n";
}
