#pragma once

#include <string>

/*
Student details
*/
struct Student
{
	std::string fName, sName; //first and last name
	int IDNum;	//student ID

	/*
	Get the student's details
	precon	- none
	postcon	- name and ID are set
	*/
	void GetDetails();

	/*
	Display a summary of the student details
	precon	- none
	postcon - details displayed onscreen
	*/
	void ShowDetails();
};
